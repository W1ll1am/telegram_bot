import aiogram.utils.markdown as md
from aiogram import Bot, Dispatcher, executor, types
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.dispatcher import FSMContext
from math import sqrt

API_TOKEN = ''
bot = Bot(token=API_TOKEN)
storage = MemoryStorage()
dp = Dispatcher(bot, storage=storage)


class Form(StatesGroup):
    begin = State()
    input_first = State()
    input_second = State()
    input = State()


def func_sum(a, b):
    if type(a) == int or type(a) == float and type(b) == int or type(b) == float:
        return a + b
    else:
        return 'Ошибка - введенные параметры не являются числами'


def func_sub(a, b):
    if type(a) == int or type(a) == float and type(b) == int or type(b) == float:
        return a - b
    else:
        return 'Ошибка - введенные параметры не являются числами'


def func_mul(a, b):
    if type(a) == int or type(a) == float and type(b) == int or type(b) == float:
        return a * b
    else:
        return 'Ошибка - введенные параметры не являются числами'


def func_div(a, b):
    if type(a) == int or type(a) == float and type(b) == int or type(b) == float:
        if b == 0:
            return 'Ошибка - деление на ноль'
        else:
            return a / b
    else:
        return 'Ошибка - введенные параметры не являются числами'


def func_sqrt(a):
    if type(a) == int or type(a) == float:
        if a < 0:
            return 'Ошибка - корень из отрицательного числа'
        else:
            return sqrt(a)
    else:
        return 'Ошибка - введенный параметр не является числом'


@dp.message_handler(commands=["start"])
async def start(m, res=False):
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    item1 = types.KeyboardButton("+")
    item2 = types.KeyboardButton("-")
    item3 = types.KeyboardButton("*")
    item4 = types.KeyboardButton("/")
    item5 = types.KeyboardButton("sqrt")
    item6 = types.KeyboardButton("end")
    markup.add(item1)
    markup.add(item2)
    markup.add(item3)
    markup.add(item4)
    markup.add(item5)
    markup.add(item6)
    await bot.send_message(m.chat.id, "Приветствуем в приложении \"Калькулятор\"\n"
                                      "Выберите операцию, которую хотите выполнить\n"
                                      "'+' - Вычислить сумму двух введенных чисел\n"
                                      "'-' - Вычислить разность двух введенных чисел\n"
                                      "'*' - Вычислить произведение двух введенных чисел\n"
                                      "'/' - Вычислить частное двух введенных чисел\n"
                                      "'sqrt' - Вычислить корень введенного числа\n"
                                      "'end' - Выйти из программы", reply_markup=markup)
    await Form.begin.set()


@dp.message_handler(state=Form.begin)
async def function_choice(message: types.Message, state: FSMContext):
    #    if message.text.strip() == '+':
    #        dp.register_next_step_handler(message, sum_func)
    markup = types.ReplyKeyboardRemove()

    if message.text.strip() == '+':
        async with state.proxy() as data:
            data['func'] = "func_sum"
            data['second_message'] = "Введите второе слагаемое"
            data['final_message'] = "Итоговая сумма: "
        await message.answer(
            text="Введите первое слагаемое",
            reply_markup=markup
        )
        await state.set_state(Form.input_first)

    if message.text.strip() == '-':
        async with state.proxy() as data:
            data['func'] = 'func_sub'
            data['second_message'] = "Введите вычитаемое"
            data['final_message'] = "Итоговая разность: "
        await message.answer(
            text="Введите уменьшаемое",
            reply_markup=markup
        )
#        await Form.input_first.set()
        await state.set_state(Form.input_first)

    if message.text.strip() == '*':
        async with state.proxy() as data:
            data['func'] = 'func_mul'
            data['second_message'] = "Введите второй множитель"
            data['final_message'] = "Итоговое произведение: "
        await message.answer(
            text="Введите первый множитель",
            reply_markup=markup
        )
        await state.set_state(Form.input_first)

    if message.text.strip() == '/':
        async with state.proxy() as data:
            data['func'] = 'func_div'
            data['second_message'] = "Введите делитель"
            data['final_message'] = "Итоговое частное: "
        await message.answer(
            text="Введите делимое",
            reply_markup=markup
        )
        await state.set_state(Form.input_first)

    if message.text.strip() == 'sqrt':
        await message.answer(
            text="Введите число из которого брать корень",
            reply_markup=markup
        )
        await state.set_state(Form.input)

    if message.text.strip() == 'end':
        await bot.send_message(message.chat.id, "До свидания! Чтобы снова воспользоваться калькулятором введите /start",
                               reply_markup=markup)
        await state.finish()


@dp.message_handler(state=Form.input_first)
async def input_first(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['val'] = message.text.strip()
        await message.answer(
            text=data['second_message']
        )
    await state.set_state(Form.input_second)


@dp.message_handler(state=Form.input_second)
async def input_second(message: types.Message, state: FSMContext):
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    item1 = types.KeyboardButton("+")
    item2 = types.KeyboardButton("-")
    item3 = types.KeyboardButton("*")
    item4 = types.KeyboardButton("/")
    item5 = types.KeyboardButton("sqrt")
    item6 = types.KeyboardButton("end")
    markup.add(item1)
    markup.add(item2)
    markup.add(item3)
    markup.add(item4)
    markup.add(item5)
    markup.add(item6)
    try:
        async with state.proxy() as data:
            await message.answer(
                text=data['final_message'] + str(eval(data['func'] + '(' + data['val'] + ', ' + message.text.strip() + ')'))
            )
        await bot.send_message(message.chat.id, "Выберите операцию, которую хотите выполнить\n"
                                                "'+' - Вычислить сумму двух введенных чисел\n"
                                                "'-' - Вычислить разность двух введенных чисел\n"
                                                "'*' - Вычислить произведение двух введенных чисел\n"
                                                "'/' - Вычислить частное двух введенных чисел\n"
                                                "'sqrt' - Вычислить корень введенного числа\n"
                                                "'end' - Выйти из программы", reply_markup=markup)
        await state.set_state(Form.begin)
    except:
        await bot.send_message(message.chat.id, "Числа были введены неправильно")
        await bot.send_message(message.chat.id, "Выберите операцию, которую хотите выполнить\n"
                                                "'+' - Вычислить сумму двух введенных чисел\n"
                                                "'-' - Вычислить разность двух введенных чисел\n"
                                                "'*' - Вычислить произведение двух введенных чисел\n"
                                                "'/' - Вычислить частное двух введенных чисел\n"
                                                "'sqrt' - Вычислить корень введенного числа\n"
                                                "'end' - Выйти из программы", reply_markup=markup)
        await state.set_state(Form.begin)


@dp.message_handler(state=Form.input)
async def input_second(message: types.Message, state: FSMContext):
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    item1 = types.KeyboardButton("+")
    item2 = types.KeyboardButton("-")
    item3 = types.KeyboardButton("*")
    item4 = types.KeyboardButton("/")
    item5 = types.KeyboardButton("sqrt")
    item6 = types.KeyboardButton("end")
    markup.add(item1)
    markup.add(item2)
    markup.add(item3)
    markup.add(item4)
    markup.add(item5)
    markup.add(item6)
    try:
        await message.answer(
            text="Корень из числа: " + str(func_sqrt(int(message.text.strip())))
        )
        await bot.send_message(message.chat.id, "Выберите операцию, которую хотите выполнить\n"
                                                "'+' - Вычислить сумму двух введенных чисел\n"
                                                "'-' - Вычислить разность двух введенных чисел\n"
                                                "'*' - Вычислить произведение двух введенных чисел\n"
                                                "'/' - Вычислить частное двух введенных чисел\n"
                                                "'sqrt' - Вычислить корень введенного числа\n"
                                                "'end' - Выйти из программы", reply_markup=markup)
        await state.set_state(Form.begin)
    except:
        await bot.send_message(message.chat.id, "Число было введено неправильно")
        await bot.send_message(message.chat.id, "Выберите операцию, которую хотите выполнить\n"
                                                "'+' - Вычислить сумму двух введенных чисел\n"
                                                "'-' - Вычислить разность двух введенных чисел\n"
                                                "'*' - Вычислить произведение двух введенных чисел\n"
                                                "'/' - Вычислить частное двух введенных чисел\n"
                                                "'sqrt' - Вычислить корень введенного числа\n"
                                                "'end' - Выйти из программы", reply_markup=markup)
        await state.set_state(Form.begin)


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)
